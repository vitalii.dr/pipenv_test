# Pipenv demo project

## Advantages of pip

* Allows to install, uninstall and update packages

* Works with versions

* Allows to install packages from **requirements.txt** file

## Disadvantages of pip

* By default install everything globally, require root rights

* Need additional tool to fix first problem (*virtualenv*, *virtualenv-wrapper*)

* Not helpful if we have a dependency conflict

* Flat dependency structure (coudn`t see dependencies of dependencies)

* To add new dependency or update old one we need to update requirements.txt manually

* There is no lock file, couldn`t repeat exact environment in other computer

### Pipfile concept

[pipfile concept repo](https://github.com/pypa/pipfile)

## How pipenv project could solve disadvantages of pip

* It use **Pipfile** and **Pipfile.lock**.
  * First file let you put the requirements that you need
  * Second file allows you to have repitable environment with exact same dependencies

* It allows you to see dependency graph

* Create virtual environment automatically

* Update **Pipfile** and **Pipfile.lock** if you install new dependency or update old one

* Checks for *security vulnerabilities* and against **PEP 508** markers

* Run commands in **virtual environment shell**

# Demo commands

## New project

```bash
$ pipenv --python 3.7

$ pipenv install requests

$ pipenv install Flask

$ pipenv install pytest --dev

$ pipenv shell
```

```python
import requests
import flask
```

```bash
$ pipenv graph

$ pipenv check
```

## How to work with existing project

```bash
$ pipenv --python 3.7

$ pipenv install

$ pipenv graph

$ pipenv run python test_env.py
```
